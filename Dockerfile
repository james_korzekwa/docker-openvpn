FROM alpine:latest

RUN apk add --no-cache openvpn curl unzip && \
	curl -sS "https://www.privateinternetaccess.com/openvpn/openvpn-strong-tcp.zip" -o /strong.zip && \
	unzip -q /strong.zip -d /pia/strong && \
	rm -f /strong.zip && \
	curl -sS "https://www.privateinternetaccess.com/openvpn/openvpn-tcp.zip" -o /normal.zip && \
	unzip -q /normal.zip -d /pia/normal && \
	rm -f /normal.zip
WORKDIR /pia
COPY openvpn.sh /usr/local/bin/openvpn.sh

ENV REGION="us_east"
ENV CONNECTIONSTRENGTH="strong"
ENTRYPOINT ["openvpn.sh"]
